FROM ubuntu:18.04
MAINTAINER FME

# Install bash, curl, python
RUN apt-get update
RUN apt-get -y install curl

# Install imagemagick
RUN apt-get -y install imagemagick

# Instal node
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -\
    && apt-get install -y nodejs

RUN node -v

CMD ['bash']